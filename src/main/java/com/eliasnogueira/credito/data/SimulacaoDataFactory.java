package com.eliasnogueira.credito.data;

import com.eliasnogueira.credito.bean.Simulacao;
import com.eliasnogueira.credito.support.CPF;
import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class SimulacaoDataFactory {

    private static Faker faker = new Faker(new Locale("pt-BR"));

    private SimulacaoDataFactory() {}

    public static Simulacao criaNovaSimulacao() {
        return Simulacao.builder().
            nome(faker.name().nameWithMiddle()).
            cpf(new CPF().generate()).
            email(faker.internet().emailAddress()).
            valor(faker.number().numberBetween(1000, 40000)).
            parcelas(faker.number().numberBetween(2, 48)).
            seguro(faker.bool().bool()).build();
    }

    public static String cpfComRestricao() {
        String[] cpfsComRestricao = {"97093236014", "60094146012", "84809766080", "62648716050",
                "26276298085", "01317496094", "55856777050", "19626829001", "24094592008", "58063164083"};

        return cpfsComRestricao[new Random().nextInt(cpfsComRestricao.length)];
    }

    public static String cpfInvalido() {
        return faker.number().digits(11);
    }

}
