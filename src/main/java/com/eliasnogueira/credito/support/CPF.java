package com.eliasnogueira.credito.support;

import java.util.Random;

/**
 * Nem tudo se cria!
 * Código retirado de https://needforjava.wordpress.com/2010/11/24/gerar-e-validar-cpf/
 */
public class CPF {

    public String generate() {
        Random r = new Random();
        StringBuilder sbCpfNumber = new StringBuilder();

        for (int i = 0; i < 9; i++) {
            sbCpfNumber.append(r.nextInt(9));
        }

        return generateDigits(sbCpfNumber.toString());
    }

    private String generateDigits(String digitsBase) {
        StringBuilder sbCpfNumber = new StringBuilder(digitsBase);
        int total = 0;
        int multiple = digitsBase.length() + 1;

        for (char digit : digitsBase.toCharArray()) {
            long parcial = Integer.parseInt(String.valueOf(digit)) * (multiple--);
            total += parcial;
        }

        int resto = Integer.parseInt(String.valueOf(Math.abs(total % 11)));

        if (resto < 2) {
            resto = 0;
        } else {
            resto = 11 - resto;
        }

        sbCpfNumber.append(resto);

        if (sbCpfNumber.length() < 11) {
            return generateDigits(sbCpfNumber.toString());
        }

        return sbCpfNumber.toString();
    }
}
