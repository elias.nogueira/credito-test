package com.eliasnogueira.credito.base;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

import static io.restassured.RestAssured.*;

public class BaseTestSimulacoes {

    @BeforeClass(alwaysRun = true)
    public void preCondicao() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        baseURI = "http://simulacoes-api.herokuapp.com";
        basePath = "/api/v1";
    }
}
