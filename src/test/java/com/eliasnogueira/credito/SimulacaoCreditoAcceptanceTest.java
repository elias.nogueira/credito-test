package com.eliasnogueira.credito;

import com.eliasnogueira.credito.bean.Simulacao;
import com.eliasnogueira.credito.data.SimulacaoDataFactory;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class SimulacaoCreditoAcceptanceTest {

    @Test(groups = {"aceitacao"})
    public void realizaSimulacaoCredito() {
        baseURI = "http://restricoes-api.herokuapp.com";
        basePath = "/api/v1";
        enableLoggingOfRequestAndResponseIfValidationFails();

        String cpf = "12345678901";

        // efetua a verificacao de cpf restrito
        given().
            pathParam("cpf", cpf).
        when().
            get("/restricoes/{cpf}").
        then().
            statusCode(204);

        // efetua a simulacao
        baseURI = "http://simulacoes-api.herokuapp.com";

        Simulacao simulacao = SimulacaoDataFactory.criaNovaSimulacao();
        given().
            contentType(ContentType.JSON).
            body(simulacao).
        when().
            post("/simulacoes").
        then().
            statusCode(201);
    }
}
