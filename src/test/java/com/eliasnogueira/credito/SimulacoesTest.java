package com.eliasnogueira.credito;

import com.eliasnogueira.credito.base.BaseTestSimulacoes;
import com.eliasnogueira.credito.bean.Simulacao;
import com.eliasnogueira.credito.data.SimulacaoDataFactory;
import com.eliasnogueira.credito.support.CPF;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.text.MessageFormat;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class SimulacoesTest extends BaseTestSimulacoes {

    @Test(groups = {"health"})
    public void healthCheck() {
        basePath = "/actuator";

        when().
            get("/health").
        then().
            statusCode(200).
            body("status", is("UP"));

        basePath = "/api/v1";
    }

    @Test(groups = {"contrato"})
    public void contratoCPFNaoExistente() {
        String cpf = new CPF().generate();

        given().
            pathParam("cpf", cpf).
        when().
            get("/simulacoes/{cpf}").
        then().
            statusCode(404).
            body(matchesJsonSchemaInClasspath("schemas/simulacoes_nao_existente_schema.json"));
    }

    @Test(groups = {"contrato"})
    public void contratoCPFExistente() {
        given().
            pathParam("cpf", "66414919004").
        when().
            get("/simulacoes/{cpf}").
        then().
            statusCode(200).
            body(matchesJsonSchemaInClasspath("schemas/simulacoes_existente_schema.json"));
    }

    @Test(groups = {"funcional"})
    public void retornaTodasSimulacoes() {
        when().
            get("/simulacoes").
        then().
            body(
                "[0].id", equalTo(1),
                "[0].nome", equalTo("Fulano"),
                "[0].cpf", equalTo("66414919004"),
                "[0].email", equalTo("fulano@gmail.com"),
                "[0].valor", equalTo(11000f),
                "[0].parcelas", equalTo(3),
                "[0].seguro", equalTo(true)
            ).body(
                "[1].id", equalTo(2),
                "[1].nome", equalTo("Deltrano"),
                "[1].cpf", equalTo("17822386034"),
                "[1].email", equalTo("deltrano@gmail.com"),
                "[1].valor", equalTo(20000f),
                "[1].parcelas", equalTo(5),
                "[1].seguro", equalTo(false)
        );
    }

    @Test(groups = {"funcional"})
    public void cPFExistente() {
        given().
            pathParam("cpf", "66414919004").
        when().
            get("/simulacoes/{cpf}").
        then().
            statusCode(200).
            body(
                "id", equalTo(1),
                "nome", equalTo("Fulano"),
                "cpf", equalTo("66414919004"),
                "email", equalTo("fulano@gmail.com"),
                "valor", equalTo(11000f),
                "parcelas", equalTo(3),
                "seguro", equalTo(true)
            );
    }

    @Test(groups = {"funcional"})
    public void efetuaSimulacao() {
        Simulacao simulacao = SimulacaoDataFactory.criaNovaSimulacao();

        given().
            contentType(ContentType.JSON).
            body(simulacao).when().
        post("/simulacoes").
            then().
        statusCode(201).
            body(
                "id", notNullValue(),
                "nome", equalTo(simulacao.getNome()),
                "cpf", equalTo(simulacao.getCpf()),
                "email", equalTo(simulacao.getEmail()),
                "valor", equalTo(simulacao.getValor()),
                "parcelas", equalTo(simulacao.getParcelas()),
                "seguro", equalTo(simulacao.getSeguro())
            );
    }

    @Test(groups = {"funcional"})
    public void cpfNaoExistente() {
        String cpf = new CPF().generate();

        given().
            pathParam("cpf", cpf).
        when().
            get("/simulacoes/{cpf}").
        then().
            statusCode(404).
            body("mensagem", equalTo(MessageFormat.format("CPF {0} não encontrado", cpf)));
    }
}
