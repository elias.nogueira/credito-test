package com.eliasnogueira.credito;

import com.eliasnogueira.credito.base.BaseTestRestricoes;
import com.eliasnogueira.credito.data.SimulacaoDataFactory;
import com.eliasnogueira.credito.support.CPF;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.io.File;
import java.text.MessageFormat;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class RestricoesTest extends BaseTestRestricoes {

    @Test(groups = "health")
    public void healthCheckViaActuator() {
        basePath = "/actuator";

        when().
            get("/health").
        then().
            statusCode(200).
            body("status", is("DOWN"));

        basePath = "/api/v1";
    }

    @Test(groups = "health")
    public void healthCheckViaAPI() {
        given().
            spec(semRestricaoSpecification()).
        when().
            get("/restricoes/{cpf}").
        then().
            statusCode(204);
    }

    @Test(groups = "contrato")
    public void contatoV1() {
        given().
            pathParam("cpf", SimulacaoDataFactory.cpfComRestricao()).
        when().
            get("/restricoes/{cpf}").
        then().
            body(
                matchesJsonSchemaInClasspath("schemas/restricoes_v1_schema.json")
            );
    }

    /**
     * Este teste irá falhar, pois está aprontando para a versão 2 da API
     * mas validando o contrato da versão 1
     *
     * Para ver a falha, mude o atributo enable para true
     */
    @Test(groups = "contrato", enabled = false)
    public void contatoV2() {
        basePath = "/api/v2";

        given().
            pathParam("cpf", SimulacaoDataFactory.cpfComRestricao()).
        when().
            get("/restricoes/{cpf}").
        then().
            body(
                matchesJsonSchemaInClasspath("schemas/restricoes_v1_schema.json")
            );
    }

    @Test(groups = "funcional")
    public void cpfSemRestricao() {
        given().
            spec(semRestricaoSpecification()).
        when().
            get("/restricoes/{cpf}").
        then().
            statusCode(204);
    }

    @Test(groups = "funcional")
    public void cpfComRestricao() {
        String cpf = SimulacaoDataFactory.cpfComRestricao();

        given().
            pathParam("cpf", cpf).
        when().
            get("/restricoes/{cpf}").
        then().
            statusCode(200).
            body("mensagem", is(MessageFormat.format("O CPF {0} possui restrição", cpf)));
    }

    /**
     * Especificação de um pathParam para gerar reaproveitamento e menos codigo nas requisições que
     * usam o parametro de path com o mesmo CPF
     */
    private RequestSpecification semRestricaoSpecification() {
        return new RequestSpecBuilder().addPathParam("cpf", new CPF().generate()).build();
    }
}
