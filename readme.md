# Projeto de Teste Credito API

Este é um projeto de teste de exemplo para demostrar a utilização do [Rest-Assured](http://rest-assured.io).

Você pode executar localmente os testes, mas deve executar as ações descritas abaixo.

## O que é necessário para executar os testes localmente

* Java JDK 8+
* Maven instalado e configurado no seu path

Também será necessário baixar as duas APIs offline:
1. Efetuar o clone do projeto [https://github.com/eliasnogueira/restricao-credito-api](https://github.com/eliasnogueira/restricao-credito-api)
2. Navegue até a pasta do projeto e execute o comando `mvn spring-boot:run`
3. Efetuar o clone do projeto [https://github.com/eliasnogueira/simulacao-credito-api](https://github.com/eliasnogueira/simulacao-credito-api)
4. Navegue até a pasta do projeto e execute o comando `mvn spring-boot:run` 

### Observação

O projeto `restricao-api` roda localmente na porta `8088` e o projeto `simulacao-api` roda na porta `8089`.

Logo será necessário você alterar o `baseURI nas classes `BaseTestSimulacoes` e `BaseTestRestricoes` e adicionar a 
propriedade `port` adicionando a respectiva porta à classe.